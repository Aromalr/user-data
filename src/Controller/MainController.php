<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Userdata;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

use PDO;
use PDOException;

class MainController extends AbstractController
{
    /**
     * @Route("/main" , name="main" , methods={"GET"})
     * 
     */
    public function index()
    {
        return $this->render('index.html.twig', [
            'controller_name' => 'USER',
        ]);

    }
   
    }
